from pcbnew import *

board = GetBoard()
for module in board.GetModules():
  ref = module.GetReference()
  x = module.GetPosition()[0]
  y = module.GetPosition()[1]
  
  # 150 mm in x and 60 mm in y
  x_boundary = 194000000
  y_boundary = 114000000
  print(ref)  
  if (ref[0] != "F" and ref[1] != "D"): # exclude fidutials
    if x > x_boundary and y > y_boundary: # fmcp 0 qsfp-dd
      module.SetReference(ref + "_0")
    elif x < x_boundary and y > y_boundary: # fmcp 1 leap
      module.SetReference(ref + "_1") 
    elif x < x_boundary and y < y_boundary: # fmcp 2 boa
      module.SetReference(ref + "_2")  
    elif x > x_boundary and y < y_boundary: # fmcp 3 ff
      module.SetReference(ref + "_3") 
